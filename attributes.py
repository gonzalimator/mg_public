"""
A module to report the attributes visible in the Channel Box and their
states. Reported data/states: All in Channel Box, Keyable, Non-keyable,
Locked, Hidden.
"""
import logging
from maya import cmds, mel

from mgUtils.decorators import undoable_func as undo


TRANS_ATTRS = ['translateX', 'translateY', 'translateZ']
ROTS_ATTRS = ['rotateX', 'rotateY', 'rotateZ']
SCL_ATTRS = ['scaleX', 'scaleY', 'scaleZ']
STD_ATTRS = TRANS_ATTRS + ROTS_ATTRS + SCL_ATTRS + ['visibility']

logger = logging.getLogger(__name__)


@undo
def attr_states(node, default_attributes=False, filter_list=None):
    """
    Return attributes found in the channelBox and their states as a
    dictionary.
    
    :param node: String. Node (name) to query attributes.
    :param default_attributes: bool. If True, return only standard attributes.
        See the global variables at the top of this module for what
        are deemed standard attributes.
    :param filter_list: list. A list of strings representing the
        default attribute types include in this function's return.
    :return: dict. The lists of attribute names organised under the
        dictionary keys: 'all', 'keyable', 'non_keyable', 'locked', 'hidden'
    """
    attr_state_dict = {}
    if not filter_list:
        filter_list = ['translate', 'rotate', 'scale', 'visibility']

    # build standard attribute list
    default_attrs = []
    default_attr_dict = {'translate': TRANS_ATTRS, 'rotate': ROTS_ATTRS,
                         'scale': SCL_ATTRS, 'visibility': ['visibility']}
    for attr in filter_list:
        default_attrs.extend(default_attr_dict[attr])

    # Gather attributes
    # Keyable
    keyable = cmds.listAttr(node, keyable=True, unlocked=True, scalar=True)
    attr_state_dict['keyable'] = keyable or []

    # Non-keyable
    non_keyable = cmds.listAttr(node, channelBox=True)
    attr_state_dict['non_keyable'] = non_keyable or []

    # Locked
    locked = cmds.listAttr(node, locked=True)  # all locked
    attr_state_dict['locked'] = locked or []

    # All in channelBox
    in_chbx = cmds.listAttr(node, keyable=True, scalar=True) or []
    if attr_state_dict['non_keyable']:
        in_chbx.extend(attr_state_dict['non_keyable'])
    attr_state_dict['all'] = in_chbx
    
    # Hidden (not necessarily locked)
    user_def = cmds.listAttr(node, userDefined=True, scalar=True)
    if in_chbx:
        hidden = list(set(default_attrs) - set(in_chbx))
        if user_def:
            hidden.extend(list(set(user_def) - set(in_chbx)))
    else:
        hidden = []
    attr_state_dict['hidden'] = hidden

    if default_attributes:
        # go through the results and filter them for standard attributes
        for k, v in attr_state_dict.iteritems():
            if v:
                sa = [attr for attr in v if attr in default_attrs]
                attr_state_dict[k] = sa

    return attr_state_dict


@undo
def copy_states(src, tgt):
    """
    Match common attribute states between given nodes.
    
    :param src: String. The name of the source node whose attributes to
                to check.
    :param tgt: The node whose attributes states will be changed to match
                the src node.
    """
    
    src_attr_dict = attr_states(src)
    
    # The dictionary keys in the order we need.
    keys = ['keyable', 'non_keyable', 'locked', 'hidden']
    for k in keys:
        if not src_attr_dict[k]:
            continue
        # Build setAttr kwarg dictionary
        kwargs = {}
        if k == 'keyable':
            kwargs['keyable'] = True
            kwargs['lock'] = False
        if k == 'non_keyable':
            kwargs['keyable'] = False
            kwargs['channelBox'] = True
        if k == 'hidden':
            kwargs['keyable'] = False
        if k == 'locked':
            kwargs['lock'] = True
            
        # Apply the attribute states 
        if src_attr_dict[k]:
            for attr in src_attr_dict[k]:
                # attributes may not exist on the tgt, so 'try'.
                try:
                    cmds.setAttr('{}.{}'.format(tgt, attr), **kwargs)
                except Exception as e:
                    logger.debug(repr(e))
