"""
A module to report the attributes visible in the Channel Box and their
states. Reported data/states: All in Channel Box, Keyable, Non-keyable,
Locked, Hidden.
"""
import logging

from pymel import core

from mgUtils.decorators import undoable_func as undo

TRANS_ATTRS = ['translateX', 'translateY', 'translateZ']
ROTS_ATTRS = ['rotateX', 'rotateY', 'rotateZ']
SCL_ATTRS = ['scaleX', 'scaleY', 'scaleZ']
STD_ATTRS = TRANS_ATTRS + ROTS_ATTRS + SCL_ATTRS + ['visibility']

logger = logging.getLogger(__name__)


def attr_states(node, default_attributes=False, default_filter=None,
                as_strings=False):
    """Return a dictionary of node attributes present in the channelBox,
    organised by attribute states.

    Attribute states are "keyable", "non_keyable", "locked" and "hidden"

    :param node: str or PyNode instance. Node to query attributes.
    :param default_attributes: bool. If True, return only standard
    attributes. See the constants at the top of this module for what are
    deemed standard attributes.
    :param default_filter: list. A list of strings representing the default
    attributes to include in this function's return. When default_attributes
    is True, filtering is available.
        Example - ['translate', 'rotate']
    :param as_strings: bool. When True, return strings instead of pymel
    Attribute() instances.
        Example - "pCube1.translateX"
    :return: dict. The lists of pymel Attribute() instances or attribute
    strings found in the channel box, organised by attribute state keys:
    'all', 'keyable', 'non_keyable', 'locked', 'hidden'
    The 'all' key contains the list of all attributes visible in the
    channel box. The 'hidden' key contains the standard and user defined
    attributes that are not in the channel box.
    """
    node = core.PyNode(node)

    # setup filtering
    default_attr_map = {'translate': TRANS_ATTRS, 'rotate': ROTS_ATTRS,
                        'scale': SCL_ATTRS, 'visibility': ['visibility']}
    if not default_filter:
        default_filter = ['translate', 'rotate', 'scale', 'visibility']
    attr_filter = []
    for attr in default_filter:
        # collect attrs for final filtered list
        attr_filter.extend(default_attr_map[attr])

    # get attribute states
    attr_state_dict = get_attr_states(node)

    # filtering
    if default_attributes:
        attr_state_dict = filter_attributes(attr_state_dict, attr_filter)
    if as_strings:
        attr_state_dict = attrs_as_strings(attr_state_dict)

    return attr_state_dict


def get_attr_states(node):
    attr_state_dict = {}
    # all attributes in channel box
    attr_state_dict['all'] = get_channel_box_attrs(node)
    # Locked
    attr_state_dict['locked'] = get_locked(node)
    # Non-keyable
    attr_state_dict['non_keyable'] = get_non_keyable(node)
    # Keyable
    attr_state_dict['keyable'] = get_keyable(node)
    # Hidden
    attr_state_dict['hidden'] = get_hidden(node)

    return attr_state_dict


def get_channel_box_attrs(node):
    # get all attributes
    all_attrs = node.listAttr()
    # all in channel box
    cb_attrs = [x for x in all_attrs if (x.isInChannelBox() or x.isKeyable())]

    return cb_attrs


def get_keyable(node):
    cb_attrs = get_channel_box_attrs(node)
    locked = get_locked(node)
    non_keyable = get_non_keyable(node)
    keyable_excluded = locked + non_keyable
    keyable = [x for x in cb_attrs if x not in keyable_excluded]

    return keyable


def get_hidden(node):
    # hidden (could be locked or not locked)
    all_attrs = node.listAttr()
    #  search standard and user defined attributes
    user_attrs = node.listAttr(userDefined=True)
    hidden_check_attrs = STD_ATTRS + [x.attrName(longName=True)
                                      for x in user_attrs]
    hidden = [x for x in all_attrs if
              x.attrName(longName=True) in hidden_check_attrs and not
              (x.isKeyable() or x.isInChannelBox())]

    return hidden


def get_non_keyable(node):
    cb_attrs = get_channel_box_attrs(node)
    non_keyable = [x for x in cb_attrs if not x.isKeyable()]

    return non_keyable


def get_locked(node):
    cb_attrs = get_channel_box_attrs(node)
    locked = [x for x in cb_attrs if x.isLocked()]

    return locked


def filter_attributes(attr_dict, filters):
    filtered_dict = {}
    # go through the attr_dict and collect attributes that match filters
    for k, v in attr_dict.items():
        if v:
            sa = [attr for attr in v if attr.name() in filters]
            filtered_dict[k] = sa

    return filtered_dict


def attrs_as_strings(attr_dict):
    str_dict = {}
    for k, v in attr_dict.items():
        str_dict[k] = []  # keep the key even if the value is empty
        if v:
            str_attrs = [x.name() for x in v]
            str_dict[k] = str_attrs

    return str_dict


@undo
def copy_states(src, tgt):
    """Match attribute states between objects.

    Get the attribute states of the src object and apply those states to
    the tgt object.

    :param src: str or PyNode instance. The source object to retrieve
    attribute states.
    :param tgt: str or PyNode instance. The target object that will receive
    the attribute states from the source object.
    """
    tgt = core.PyNode(tgt)

    # get src attr states
    src_states = attr_states(src)

    # The order in which we need to process attr states.
    # First, use the 'all' key to make sure all attrs that should be in the
    # channelBox are added. Then process the rest of the states in order.
    ordered_states = ['all', 'keyable', 'non_keyable', 'locked', 'hidden']

    for state in ordered_states:
        src_attrs = src_states.get(state, [])
        if not src_attrs:
            continue

        # get the pymel Attribute methods needed to set the current state
        method_states = get_attr_methods_from_state(state)

        # get the src attributes with current state and set the tgt
        #  attributes' state
        for attr in src_attrs:
            # skip attr if not on tgt node
            if not tgt.hasAttr(attr.attrName()):
                continue
            # get tgt Attribute class instance
            tgt_attr = tgt.attr(attr.attrName())
            for method, value in method_states:
                try:
                    # get bound method from method name
                    tgt_method = getattr(tgt_attr, method)
                    tgt_method(value)
                except Exception as e:
                    logger.debug(repr(e))
                    print("Failed to set attribute: {} to {}"
                          .format(tgt_attr, value))


def get_attr_methods_from_state(state):
    """Return pymel Attribute method names with necessary arguments
    needed to set an attribute's state to the given state.

    :param state: str. A known term for attribute states used in this
    module. Valid terms - 'all', 'keyable', 'non_keyable', 'locked', 'hidden'
    :return: list. A list of two-item tuples.
        Example - [('setKeyable', False), ('showInChannelBox', True)]
    """
    if state == 'all':
        method_states = [('setKeyable', True)]
    elif state == 'keyable':
        method_states = [('setLocked', False)]
    elif state == 'non_keyable':
        method_states = [('setLocked', False), ('setKeyable', False),
                         ('showInChannelBox', True)]
    elif state == 'locked':
        method_states = [('setLocked', True)]
    elif state == 'hidden':
        method_states = [('setKeyable', False)]
    else:
        logger.info("Attribute state '{}' not recognized.".format(state))
        return

    return method_states
