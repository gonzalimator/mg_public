from maya import OpenMayaUI
from shiboken import wrapInstance

from PySide import QtGui, QtCore
from mgUtils.qt.qt_widgets import BaseWindow
from mgUtils.rig import selection_utils as su
from mgUtils.rig import isolate as iso


WIN = None
DEFAULT_COLUMNS = ['Asset', 'Head', 'Torso', 'L Arm', 'R Arm', 'L Leg', 'R Leg']

def maya_window():
    ptr = OpenMayaUI.MQtUtil.mainWindow()
    return wrapInstance(long(ptr), QtGui.QWidget)


def main():
    global WIN
    if not WIN:
        WIN = IsolateUI(parent=maya_window())
        WIN.show()
        return WIN
    else:
        WIN.showNormal()
        WIN.activateWindow()
        return WIN


class IsoTable(QtGui.QTableView):
    def __init__(self, model=None):
        super(IsoTable, self).__init__()

        self.model = model
        self.header = self.horizontalHeader()
        self.best_column_width = 40

        self.init_ui()

    def init_ui(self):
        # set the model
        self.setModel(self.model)

        # appearance
        self.verticalHeader().hide()
        self.setShowGrid(False)

        # fix the table background so that it is not so dark (checkboxes not
        #  really visible otherwise)
        self.setAutoFillBackground(True)
        palette = self.palette()
        palette.setColor(QtGui.QPalette.Base,
                         palette.color(QtGui.QPalette.Window))
        self.setPalette(palette)

        # set up columns.
        self.set_column_delegates()

        # blank out non applicable cells
        self.blank_cells()

        # set the table width, now that the columns are set up.
        self.set_width()

    def set_column_delegates(self):
        for i in range(1, len(self.model.columns)):
            self.setItemDelegateForColumn(i, CheckBoxDelegate(self))

    def blank_cells(self):
        indexes = self.model.audit_cells()
        if indexes:
            for idx in indexes:
                blank = QtGui.QPushButton("N/A")
                blank.setEnabled(False)
                self.setIndexWidget(idx, blank)

    def set_width(self):
        """Resize the table width based on the table's contents.

        Resize all columns to their optimal width. Measure the table's
        header length, scrollbar width, and frame width. Set the table's
        width to the total of those measurements.
        """
        # resize columns
        self.resizeColumnsToContents()
        # determine the best column width
        self.best_column_width = 40  # minimum width
        for i in range(1, len(self.model.columns)):
            test_width = self.columnWidth(i)
            if test_width > self.best_column_width:
                self.best_column_width = test_width
        # set columns to the best width
        for i in range(1, len(self.model.columns)):
            self.setColumnWidth(i, self.best_column_width)

        # measurements
        header_width = self.horizontalHeader().length()
        scroll_width = self.style().pixelMetric(
            QtGui.QStyle.PM_ScrollBarExtent)
        frame_width = self.frameWidth() * 2
        new_width = header_width + scroll_width + frame_width

        # set the table width
        self.setMinimumWidth(new_width)


class IsoModel(QtGui.QStandardItemModel):
    def __init__(self, assets, app_config, headers=None):
        super(IsoModel, self).__init__()

        # assets; these are the assets that have descriptions
        self.assets = assets  # asset namespaces

        # app config - used for audit_cells
        self.app_config = app_config

        # header storage/set up
        self.columns = headers or DEFAULT_COLUMNS

        self.setHorizontalHeaderLabels(self.columns)

        # data storage
        self.iso_data = {}

        # build the model items
        self.build_model(self.assets)

    def row_count(self):
        return len(self.assets)

    def column_count(self):
        return len(self.columns)

    def build_model(self, assets):
        """Build the model's items based on the provided assets.

        We need to pass the assets in order to be able update the assets
        after the model has been instantiated.

        :param assets: list. The list of character namespaces to display.
        """
        # update the assets in case they've changed
        self.assets = assets
        rows = self.row_count()
        columns = self.column_count()
        self.setRowCount(rows)
        self.setColumnCount(columns)

        # add all items to model
        for i, ast in enumerate(self.assets):
            for column in range(columns):
                if 0 == column:
                    # asset namespace column
                    item = QtGui.QStandardItem(ast)
                    self.setItem(i, column, item)
                else:
                    item = QtGui.QStandardItem()
                    self.setItem(i, column, item)
                # set all items to non-selectable
                item.setSelectable(False)

        # save the data
        self.store_data()

    def audit_cells(self):
        """Get the asset data and blank out cells for groups that don't apply.
        """
        cells_to_blank = []
        # if no isolatable assets, blank all cells
        if "No_Isolatable_Assets" in self.assets:
            for i in range(1, len(self.columns)):
                idx = self.index(0, i)
                cells_to_blank.append(idx)

            return cells_to_blank

        unique_data = self.app_config.get('unique', {})

        for row in range(self.row_count()):
            for i, col in enumerate(self.columns):
                # skip if the asset column
                if i == 0:
                    continue
                # skip if the column is a standard column common to all assets
                if col in self.app_config.get('groups', {}):
                    continue

                idx = self.index(row, i)  # current cell
                asset_idx = self.index(row, 0)  # asset cell

                # check the 'unique' dict key for the asset, then check if it
                #  contains the column name
                asset_name = su.get_char_name(self.data(asset_idx))
                unq_groups = unique_data.get(asset_name, {})

                if asset_name not in unique_data:
                    cells_to_blank.append(idx)
                    continue
                else:
                    if col not in unq_groups:
                        cells_to_blank.append(idx)

                # if 'tail' in col and 'cat' not in self.data(asset_idx):
                #     cells_to_blank.append(idx)

        return cells_to_blank

    def store_data(self):
        """Clear any existing data and store the current data from the
        model.

        This builds a default dictionary in self.iso_data.
        """
        self.iso_data.clear()
        for i, ast in enumerate(self.assets):
            state_list = []
            for col in range(1, self.column_count()):
                state = self.item(i, col).checkState()
                bool_state = True if state == QtCore.Qt.Checked else False
                state_list.append(bool_state)
            self.iso_data[ast] = state_list

    def setData(self, index, value, role):
        """Override built-in setData to update item checkStates as
        booleans in self.iso_data.

        :param index: QtCore.QModelIndex
        :param value: QtCore.Qt.CheckState
        :param role: QtCore.Qt.ItemDataRole
        :return: bool. If data is set.
        """
        if role == QtCore.Qt.EditRole:
            row = index.row()
            column = index.column()
            asset = self.item(row, 0).text()  # column 0 for asset namespace
            state = True if value == QtCore.Qt.CheckState.Checked else False

            if asset in self.iso_data:
                self.iso_data[asset][column-1] = state
                super(IsoModel, self).setData(index, value, role)
                return True
            else:
                super(IsoModel, self).setData(index, value, role)
                return False

    def reset_checked(self):
        """Set all checkboxes in all rows to unchecked."""
        for row in range(self.row_count()):
            for column in range(1, self.column_count()):
                idx = self.index(row, column)
                self.setData(idx, QtCore.Qt.Unchecked, QtCore.Qt.EditRole)

    def prep_data(self):
        """Translate the self.iso_data dictionary values to strings that
        iso.isolate() needs and return the translated dictionary.

        The parts list contains the strings that iso.isolate() wants, in
        the same order as the corresponding columns in the UI. We zip this
        with the boolean list for each asset in self.iso_data and if we
        encounter a True value, we add it to the new translated dictionary.

        See help(iso.isolate) for the dictionary format expected and that
        is returned.
        """
        # parts = ['head', 'torso', 'l_arm', 'r_arm', 'l_leg', 'r_leg']
        parts = self.columns[1:]
        func_arg = {}
        for asset, values in self.iso_data.items():
            parts_arg = []
            for part, val in zip(parts, values):
                if val:
                    parts_arg.append(part)
            if parts_arg:
                func_arg[asset] = parts_arg

        return func_arg


class CheckBoxDelegate(QtGui.QStyledItemDelegate):
    """
    A delegate that places a fully functioning QCheckBox in every
    cell of the column to which it's applied. (Stolen from StackExchange!)
    """
    sig_data_changed = QtCore.Signal(QtCore.QModelIndex)

    def __init__(self, parent=None):
        QtGui.QStyledItemDelegate.__init__(self, parent=parent)

    def createEditor(self, parent, option, index):
        """
        Important, otherwise an editor is created if the user clicks in
        this cell.
        ** Need to hook up a signal to the model
        """
        return None

    def paint(self, painter, option, index):
        """
        Paint a checkbox without the label.
        """

        checked = index.data()
        check_box_style_option = QtGui.QStyleOptionButton()
        # handle enabled or read only
        if (index.flags() & QtCore.Qt.ItemIsEditable) > 0:
            check_box_style_option.state |= QtGui.QStyle.State_Enabled
        else:
            check_box_style_option.state |= QtGui.QStyle.State_ReadOnly
        # handle state
        if checked:
            check_box_style_option.state |= QtGui.QStyle.State_On
        else:
            check_box_style_option.state |= QtGui.QStyle.State_Off

        check_box_style_option.rect = self.getCheckBoxRect(option)
        check_box_style_option.state |= QtGui.QStyle.State_Enabled

        # draw the checkbox
        QtGui.QApplication.style().drawControl(QtGui.QStyle.CE_CheckBox,
                                               check_box_style_option, painter)

    def editorEvent(self, event, model, option, index):
        """
        Change the data in the model and the state of the checkbox
        if the user presses the left mouse button or presses
        Key_Space or Key_Select and this cell is editable. Otherwise
        do nothing.
        """
        if not (index.flags() & QtCore.Qt.ItemIsEditable) > 0:
            return False

        # Do not change the checkbox-state for the following events
        if event.type() == QtCore.QEvent.MouseButtonPress:
            return False

        if event.type() == QtCore.QEvent.MouseMove:
            return False
        if event.type() == QtCore.QEvent.MouseButtonRelease or \
                event.type() == QtCore.QEvent.MouseButtonDblClick:
            if event.button() != QtCore.Qt.LeftButton or not \
                    self.getCheckBoxRect(option).contains(event.pos()):
                return False
            if event.type() == QtCore.QEvent.MouseButtonDblClick:
                return False
        elif event.type() == QtCore.QEvent.KeyPress:
            if event.key() != QtCore.Qt.Key_Space and \
                    event.key() != QtCore.Qt.Key_Select:
                return False
            else:
                return False

        # Change the checkbox-state
        self.setModelData(None, model, index)
        return True

    def setModelData (self, editor, model, index):
        """
        For the given model, toggle the given index's check state to the
        opposite of its current state.
        """
        new_value = QtCore.Qt.Checked if not \
            index.data() else QtCore.Qt.Unchecked
        model.setData(index, new_value, QtCore.Qt.EditRole)

    def getCheckBoxRect(self, option):
        check_box_style_option = QtGui.QStyleOptionButton()
        check_box_rect = QtGui.QApplication.style().subElementRect(
            QtGui.QStyle.SE_CheckBoxIndicator, check_box_style_option, None)
        check_box_point = QtCore.QPoint(option.rect.x() +
                                        option.rect.width() / 2 -
                                        check_box_rect.width() / 2,
                                        option.rect.y() +
                                        option.rect.height() / 2 -
                                        check_box_rect.height() / 2)
        return QtCore.QRect(check_box_point, check_box_rect.size())


class IsolateUI(BaseWindow):
    def __init__(self, assets=None, parent=None):
        super(IsolateUI, self).__init__(parent=parent, app_name='maya')
        self.app_config = iso.load_app_config()
        self.assets = assets
        if not self.assets:
            self.get_assets()

        # assets that have isolate descriptions
        self.iso_assets = {}
        self.get_isolatable()

        # self.iso_groups = self.app_config['groups']
        self.iso_groups = self.table_headers()

        self.model = IsoModel(self.iso_assets, self.app_config,
                              headers=self.iso_groups)
        self.table = IsoTable(self.model)
        self.table_hbox = QtGui.QHBoxLayout()

        self.btn_box = QtGui.QHBoxLayout()
        self.refresh_btn = QtGui.QPushButton('Refresh UI')
        self.iso_btn = QtGui.QPushButton('Isolate')
        self.clear_btn = QtGui.QPushButton('Clear Boxes')

        self.init_ui()

    def init_ui(self):
        # table setup
        self.table_hbox.addStretch()
        self.table_hbox.addWidget(self.table)
        self.table_hbox.addStretch()
        # INFO: self.main_layout is inherited from BaseWindow
        self.main_layout.addLayout(self.table_hbox)

        # button setup
        self.refresh_btn.setMinimumWidth(90)
        self.iso_btn.setMinimumWidth(90)
        self.clear_btn.setMinimumWidth(90)
        self.btn_box.addWidget(self.refresh_btn)
        self.btn_box.addStretch()
        self.btn_box.addWidget(self.clear_btn)
        self.btn_box.addWidget(self.iso_btn)
        self.main_layout.addLayout(self.btn_box)

        self.connections()
        self.button_state()

    def button_state(self):
        if 'No_Isolatable_Assets' in self.iso_assets:
            self.iso_btn.setEnabled(False)
        else:
            self.iso_btn.setEnabled(True)

    def connections(self):
        self.refresh_btn.clicked.connect(self.refresh)
        self.iso_btn.clicked.connect(self.isolate)
        self.clear_btn.clicked.connect(self.clear_selection)

    def clear_selection(self):
        self.model.reset_checked()

    def isolate(self):
        """Call iso.isolate() or iso.integrate().

        If there are checked items in the UI, call iso.isolate().
        If nothing is checked in the UI, then call iso.integrate().
        """
        args = self.model.prep_data()
        if not args:
            iso.integrate()
        else:
            iso.isolate(args)

    def table_headers(self):
        headers = self.app_config['groups'][:]
        unique = self.get_unique_groups()
        if unique:
            headers.extend(unique)

        headers.insert(0, 'Asset')

        return headers

    def get_unique_groups(self):
        unique = []
        # get current assets to see if any unique isolate groups/columns
        #  need to be loaded.
        unique_dict = self.app_config.get('unique', [])
        if unique_dict:
            for asset, groups in unique_dict.items():
                if asset not in self.assets.values():
                    # don't load this assets' unique isolate groups
                    continue
                unique.extend(groups.keys())

        return unique

    def get_assets(self):
        self.assets = su.get_characters()
        if not self.assets:
            self.assets = {'No_Isolatable_Assets': 'Name'}

    def get_isolatable(self):
        # clear the dictionary
        self.iso_assets.clear()

        if 'No_Isolatable_Assets' in self.assets:
            self.iso_assets = self.assets
            return
        # check for asset descriptions
        for asset, name in self.assets.items():
            if not iso.file_check(asset):
                print "# No Isolate data found for asset " \
                      "'{}'.\n".format(asset),
            else:
                self.iso_assets.update({asset: name})

        if not self.iso_assets:
            self.iso_assets = {'No_Isolatable_Assets': 'Name'}

    def refresh(self):
        """Rebuild the self.model with discovered character assets in
        the scene.

        Clear the stored data in the model. Find any character assets in
        the scene and pass them to self.model.build_model(). Adjust the
        size of the table to fit the new model's items.
        """
        # check for changes in config
        self.app_config = iso.load_app_config()

        # clear the data and prepare new data
        self.model.iso_data.clear()
        self.get_assets()
        self.get_isolatable()

        # rebuild the model/table
        headers = self.table_headers()
        self.model.columns = headers
        self.model.setHorizontalHeaderLabels(headers)
        self.model.build_model(self.iso_assets)  # isolatable assets
        self.table.set_column_delegates()
        self.table.blank_cells()
        self.table.set_width()
        self.button_state()
